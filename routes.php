<?php
// register routes
$router->map('GET', '/register', 'Project\Controllers\RegisterController@getShowRegisterPage', 'register');
$router->map('POST', '/register', 'Project\Controllers\RegisterController@postShowRegisterPage', 'register_post');

// review routes
$router->map('GET', '/reviews', 'Project\Controllers\ReviewController@getShowReviews', 'reviews');


// logged in user routes
if (Project\Auth\LoggedIn::user()) {
  $router->map('GET', '/add-review', 'Project\Controllers\ReviewController@getShowAdd', 'add_review');
  $router->map('POST', '/add-review', 'Project\Controllers\ReviewController@postShowAdd', 'add_review_post');
}

// login/logout routes
$router->map('GET', '/login', 'Project\Controllers\AuthenticationController@getShowLoginPage', 'login');
$router->map('POST', '/login', 'Project\Controllers\AuthenticationController@postShowLoginPage', 'login_post');
$router->map('GET', '/logout', 'Project\Controllers\AuthenticationController@getLogout', 'logout');




// page routes
$router->map('GET', '/', 'Project\Controllers\PageController@getShowHomePage', 'home');
$router->map('GET', '/page-not-found', 'Project\Controllers\PageController@getShow404', '404');
$router->map('GET', '/[*]', 'Project\Controllers\PageController@getShowPage', 'generic_page');
